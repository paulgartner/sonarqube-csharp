# Sonarqube in docker with c# development plugins
Included plugins:
* java 4.6.0.8784 (required by csharp plugin) 
* csharp 5.8.0.660 
* clover 3.1 
* fxcop 1.0 


### Run with embedded database
`docker run -d -p 9000:9000 --name sonarqube-csharp paulgartner/sonarqube-csharp`


### Other software
1. FxCop 
 * This should be installed with Visual Studio 2013 `C:/Program Files (x86)/Microsoft Visual Studio 12.0/Team Tools/Static Analysis Tools/FxCop/FxCopCmd.exe`
2. VSSonarExtension 
 * VisualStudio extension installable from Tools -> Extensions and Updates


### Sonarqube Setup
1. Browse to http://localhost:9000/
2. Login as admin: 
  1. Default credentials; admin:admin 
  2. Change the admin password if desired 
  3. http://localhost:9000/account/security 
3. Create a new user for SonarScanner 
  * User should be part of the **sonar-users** group 
  * http://localhost:9000/users 
4. Update plugins 
  * http://localhost:9000/updatecenter/installed 
5. Update path to FxCopCmd.exe 
  * http://localhost:9000/settings/?category=fxcop 


### VSSonarExtension setup
1. Menu bar, Sonar -> Configuration 
  * Server Address: http://localhost:9000 
  * Login or Token, use settings from set 3 in sonarqube setup 
  * test connection + connect to server 
  * Save and exit 
2. Right click solution ->  SQ: Run Full Analysis 
  * If asked to provision a new project, click ok 
3. Fill in the form 
4. Click ok 
 

### Plugins
Plugin sources:
* https://sonarsource.bintray.com/Distribution/sonar-java-plugin/sonar-java-plugin-4.6.0.8784.jar
* https://sonarsource.bintray.com/Distribution/sonar-csharp-plugin/sonar-csharp-plugin-5.8.0.660.jar
* https://sonarsource.bintray.com/Distribution/sonar-clover/sonar-clover-plugin-3.1.jar
* https://github.com/SonarQubeCommunity/sonar-fxcop/releases/download/1.0/sonar-fxcop-plugin-1.0.jar