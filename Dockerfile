FROM sonarqube

# java plugin
RUN curl -sLo /opt/sonarqube/extensions/plugins/sonar-java-plugin-4.6.0.8784.jar     https://sonarsource.bintray.com/Distribution/sonar-java-plugin/sonar-java-plugin-4.6.0.8784.jar

# csharp plugin
RUN curl -sLo /opt/sonarqube/extensions/plugins/sonar-csharp-plugin-5.8.0.660.jar    https://sonarsource.bintray.com/Distribution/sonar-csharp-plugin/sonar-csharp-plugin-5.8.0.660.jar

# clover plugin
RUN curl -sLo /opt/sonarqube/extensions/plugins/sonar-clover-plugin-3.1.jar          https://sonarsource.bintray.com/Distribution/sonar-clover/sonar-clover-plugin-3.1.jar

# fxcop plugin
RUN curl -sLo /opt/sonarqube/extensions/plugins/sonar-fxcop-plugin-1.0.jar           https://github.com/SonarQubeCommunity/sonar-fxcop/releases/download/1.0/sonar-fxcop-plugin-1.0.jar

# port binding
EXPOSE 9000
